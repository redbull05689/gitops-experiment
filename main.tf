terraform {
  backend "http" {}
  required_providers {
      amixr = {
        source = "alertmixer/amixr"
        version = "0.2.0"
      }
      digitalocean = {
        source = "digitalocean/digitalocean"
        version = "1.22.2"
      }
    }
}

// DO
variable "digitalocean_token" {
  type = string
}

provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_kubernetes_cluster" "democluster" {
  name    = "democluster"
  region  = "fra1"
  version = "1.18.6-do.0"

  node_pool {
    name       = "worker-pool"
    size       = "s-1vcpu-2gb"
    node_count = 1
  }
}

// Amixr
//variable "amixr_token" {
//  type = string
//}
//
//provider "amixr" {
//  token = var.amixr_token
//}

//provider "statuscake" {
//  username = "testuser"
//  apikey   = "12345ddfnakn"
//}
//
//resource "statuscake_test" "gitopswebinar" {
//  website_name = "gitopswebinar"
//  website_url  = "IP"
//  test_type    = "HTTP"
//  check_rate   = 300
//  contact_group = ["amixr_gitops_webinar"]
//}